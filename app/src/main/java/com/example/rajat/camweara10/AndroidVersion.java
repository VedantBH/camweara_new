package com.example.rajat.camweara10;

/**
 * Created by rajat on 12/8/17.
 */

public class AndroidVersion {
    private String recyclerViewTitleText;
    private int recyclerViewImage;
    private String productType = "none";

    public String getrecyclerViewTitleText() {
        return recyclerViewTitleText;
    }

    public void setAndroidVersionName(String recyclerVietTitleText) {
        this.recyclerViewTitleText = recyclerVietTitleText;
    }

    public int getrecyclerViewImage() {
        return recyclerViewImage;
    }

    public void setrecyclerViewImage(int recyclerViewImage) {
        this.recyclerViewImage = recyclerViewImage;
    }

    public void setProductType(String productType){
        this.productType = productType;
    }

    public String getProductType(){
        return productType;
    }
}
