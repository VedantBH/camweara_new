package com.example.rajat.camweara10;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by rajat on 12/8/17.
 */

public class AndroidDataAdapter extends RecyclerView.Adapter<AndroidDataAdapter.ViewHolder> {

    private ArrayList<AndroidVersion> arrayList;
    private Context mcontext;

    public AndroidDataAdapter(Context context, ArrayList<AndroidVersion> android) {
        this.arrayList = android;
        this.mcontext = context;
    }

    @Override
    public void onBindViewHolder(AndroidDataAdapter.ViewHolder holder, final int i) {

        holder.textView.setText(arrayList.get(i).getrecyclerViewTitleText());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Added to cart", Toast.LENGTH_SHORT).show();
            }
        });

        //holder.imageView.setImageResource(arrayList.get(i).getrecyclerViewImage());
        Glide.with(this.mcontext)
                .load(arrayList.get(i).getrecyclerViewImage())
                .placeholder(R.drawable.loading)
                .into(holder.imageView);

        final DisplayMetrics displayMetrics = mcontext.getResources().getDisplayMetrics();
        holder.imageView.setMaxHeight(displayMetrics.heightPixels/4);
        holder.imageView.setMinimumHeight(displayMetrics.heightPixels/4);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "AR preview", Toast.LENGTH_SHORT).show();
                int imageName = arrayList.get(i).getrecyclerViewImage();
                Intent xIntent = new Intent(view.getContext(), FaceFilterActivity.class);
                xIntent.putExtra("image", imageName);
                xIntent.putExtra("Type", arrayList.get(i).getProductType());
                view.getContext().startActivity(xIntent);
            }
        });
    }

    @Override
    public AndroidDataAdapter.ViewHolder onCreateViewHolder(ViewGroup vGroup, int i) {

        View view = LayoutInflater.from(vGroup.getContext()).inflate(R.layout.row_layout, vGroup, false);
        return new ViewHolder(view);
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private ImageView imageView;

        public ViewHolder(View v) {
            super(v);

            textView = (TextView) v.findViewById(R.id.text);
            imageView = (ImageView) v.findViewById(R.id.image);
        }
    }
}
