package com.example.rajat.camweara10;

/**
 * Created by VEDAB on 9/17/2017.
 */

public class Constants {

    public static final String NecklaceType = "Necklace";
    public static final String TieType = "Tie";
    public static final String TshirtType = "Tees";
    public static final String EarRingType = "EarRing";
    public static final String JacketType ="Jackets";
}
