/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.rajat.camweara10;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.Log;

import com.example.rajat.camweara10.ui.camera.GraphicOverlay;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
class FaceGraphic extends GraphicOverlay.Graphic {
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float GENERIC_POS_OFFSET = 20.0f;
    private static final float GENERIC_NEG_OFFSET = -20.0f;

    private static final float BOX_STROKE_WIDTH = 5.0f;

    private static final int COLOR_CHOICES[] = {
            Color.BLUE,
            Color.CYAN,
            Color.GREEN,
            Color.MAGENTA,
            Color.RED,
            Color.WHITE,
            Color.YELLOW
    };
    private static int mCurrentColorIndex = 0;

    private Paint mFacePositionPaint;
    private Paint mIdPaint;
    private Paint mBoxPaint;

    private volatile Face mFace;
    private int mFaceId;
    private float mFaceHappiness;
    private Bitmap bitmap;
    /*private Bitmap bitmapRing;
    private Bitmap bitmapBindi;
    private Bitmap bitmapTee;*/
    private Bitmap op;
    private Bitmap opRing;
    private Bitmap opBindi;
    private String TAG = "FaceGraphic";
    private String productType = "none";

    public void updateBitmap(int imageName){
        bitmap = BitmapFactory.decodeResource(getOverlay().getContext().getResources(), imageName);
        op = bitmap;
    }

    FaceGraphic(GraphicOverlay overlay, int imageName, String productType) {
        super(overlay);

        Log.d(TAG, "imageName " + imageName);
        Log.d(TAG,"product Type: "+ productType);
        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        mFacePositionPaint = new Paint();
        mFacePositionPaint.setColor(selectedColor);

        mIdPaint = new Paint();
        mIdPaint.setColor(selectedColor);
        mIdPaint.setTextSize(ID_TEXT_SIZE);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(selectedColor);
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
        this.productType = productType;
        bitmap = BitmapFactory.decodeResource(getOverlay().getContext().getResources(), imageName);
       /* bitmapRing = BitmapFactory.decodeResource(getOverlay().getContext().getResources(), R.drawable.ear_ring);
        bitmapBindi = BitmapFactory.decodeResource(getOverlay().getContext().getResources(), R.drawable.bindi1);
        bitmapTee = BitmapFactory.decodeResource(getOverlay().getContext().getResources(), R.drawable.tshirt2);*/
        op = bitmap;
        /*opRing = bitmapRing;
        opBindi = bitmapBindi;*/
    }

    void setId(int id) {
        mFaceId = id;
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateFace(Face face) {
        mFace = face;
        float leftEye = 0;
        float rightEye = 0;
        float diff = 0;
        float x = translateX(face.getPosition().x + face.getWidth() / 2);
        float y = translateY(face.getPosition().y + face.getHeight() / 2);
        float xOffset = scaleX(face.getWidth() / 2.0f);
        float yOffset = scaleY(face.getHeight() / 2.0f);
        float left = x - xOffset;
        float top = y - yOffset;
        float right = x + xOffset;
        float bottom = y + yOffset;

        float faceRectWidth = right - left - 100;

        for (Landmark landmark : face.getLandmarks()) {
            switch (landmark.getType()) {
                case Landmark.LEFT_EYE:
                    leftEye = translateX(landmark.getPosition().x);
                    break;
                case Landmark.RIGHT_EYE:
                    rightEye = translateX(landmark.getPosition().x);
                    break;
            }
        }
        diff = rightEye - leftEye;
        switch (productType){
            case Constants.NecklaceType:
                op = Bitmap.createScaledBitmap(bitmap, (int)(faceRectWidth),
                        (int) scaleY(((bitmap.getHeight() * face.getWidth()) / bitmap.getWidth())/2), false);
                break;

            case Constants.EarRingType:
                op = Bitmap.createScaledBitmap(bitmap, (int) scaleX(face.getWidth()/ 10),
                        (int)scaleY(((bitmap.getHeight() * face.getWidth()) / bitmap.getWidth())/10), false);
                break;

            case Constants.TieType:
                op = Bitmap.createScaledBitmap(bitmap, (int) scaleX(diff/2),
                        (int) scaleY(((bitmap.getHeight() * face.getWidth()) / bitmap.getWidth())/3), false);
                break;

            case Constants.TshirtType:
                Log.d(TAG," bitmapTee width & height "+ bitmap.getWidth()+ " & "+ bitmap.getHeight());
                /*op = Bitmap.createScaledBitmap(bitmap, (int) scaleX(face.getWidth()),
                        (int)scaleY(bitmap.getHeight()), false);*/
                op = Bitmap.createScaledBitmap(bitmap, (int) scaleX(face.getWidth()*3),
                        750, false);
                //TODO: (int)scaleY(bitmap.getHeight()) should be 750...check the issue
                Log.d(TAG,"scaleY(bitmap.getHeight)"+(int)scaleY(bitmap.getHeight()));
                break;
        }

       /* opRing = Bitmap.createScaledBitmap(bitmapRing, (int) scaleX(face.getWidth()/ 10),
                (int)scaleY(((bitmap.getHeight() * face.getWidth()) / bitmap.getWidth())/10), false);

        opBindi = Bitmap.createScaledBitmap(bitmapBindi, (int) scaleX(face.getWidth()/15),
                (int)scaleY(((bitmap.getHeight() * face.getWidth()) / bitmap.getWidth())/15), false);*/

        /*op = Bitmap.createScaledBitmap(bitmap, (int)(diff),
                (int) scaleY(((bitmap.getHeight() * face.getWidth()) / bitmap.getWidth())/2), false);*/

        postInvalidate();
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        Face face = mFace;

        if (face == null) {
            return;
        }

        // Draws a circle at the position of the detected face, with the face's track id below.
        float x = translateX(face.getPosition().x + face.getWidth() / 2);
        float y = translateY(face.getPosition().y + face.getHeight() / 2);
        float xOffset = scaleX(face.getWidth() / 2.0f);
        float yOffset = scaleY(face.getHeight() / 2.0f);
        float left = x - xOffset;
        float top = y - yOffset;
        float right = x + xOffset;
        float bottom = y + yOffset;
        float leftEyeX = 0;
        float diff = right - left - 100;
        boolean isLeftEyeDetected  = false;
        boolean isRightEyeDetected = false;
        float noseX = 0;

        for (Landmark landmark : face.getLandmarks()) {

            //canvas.drawCircle(lX, lY, 2, mBoxPaint);
            if (landmark.getType() == Landmark.LEFT_CHEEK){
                //canvas.drawBitmap(opRing, left+50, lY, new Paint());
            }

            else if (landmark.getType() == Landmark.NOSE_BASE){
                noseX = translateX(landmark.getPosition().x);
            }

            else if (landmark.getType() == Landmark.RIGHT_EYE){
                isRightEyeDetected = true;
            }

            else if (landmark.getType() == Landmark.LEFT_EYE){
                leftEyeX = translateX(landmark.getPosition().x);
                isLeftEyeDetected = true;
            }
        }
        switch (productType){
            case Constants.NecklaceType:
                canvas.drawBitmap(op, left+50, bottom, new Paint());
                break;

            case Constants.EarRingType:
                break;

            case Constants.TieType:
                if (isLeftEyeDetected && isRightEyeDetected){
                    canvas.drawBitmap(op, leftEyeX-130, bottom + 30, new Paint());
                    //canvas.drawBitmap(op, leftEyeX-20, bottom + 30, new Paint());
                }
                break;

            case Constants.TshirtType:
                Log.d(TAG,"TshirtType drawBitmap");
                Log.d(TAG,"op width & height "+ op.getWidth()+" & "+ op.getHeight());
                if(noseX != 0)
                    canvas.drawBitmap(op, noseX - (int) scaleX(face.getWidth()*3)/2, bottom-60, new Paint());
                break;
        }

        //canvas.drawBitmap(opBindi, noseX, leftEyeY - 50, new Paint());

    }
}
